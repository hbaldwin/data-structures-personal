/**********************************************
* File: BTree.h
* Author: Hope Baldwin
* Email: hbaldwi2@nd.edu
*  
* The main header file which constructs a B-Tree 
* as presented in Notre Dame CSE 20312 Data Structures
**********************************************/
#ifndef BTREE_H
#define BTREE_H

#include "BTreeNode.h"

template<class T>
class BTree 
{ 
    BTreeNode<T> *root; // Pointer to root node 
    int t;  			// Degree 
	
public:  
    /********************************************
    * Function Name  : t
    * Pre-conditions : _t
    * Post-conditions: BTree<T>
    *  
	* Constructor (Initializes tree as empty) 
    ********************************************/
    BTree<T>(int _t) : root(nullptr), t(_t) { }
  
    /********************************************
    * Function Name  : traverse
    * Pre-conditions : none
    * Post-conditions: none
    * 
	* function to traverse the tree 
    ********************************************/
    void traverse(){  
		if (root != NULL) 
			root->traverse(); 
	} 
  
    /********************************************
    * Function Name  : search
    * Pre-conditions : T key
    * Post-conditions: BTreeNode<T>*
    *  
	* function to search a key in this tree 
    ********************************************/
    BTreeNode<T>* search(T key){  
		return (root == NULL)? NULL : root->search(key); 
	} 
    /********************************************
    *Function Name  : printFoundNodes
    * Pre-conditions : T key
    * Post-conditions: void
    	* function to print the elements of each node in a path
    *********************************************/
    void printFoundNodes(T key){
		root->printFoundNodes(key, 1);
    }
	// The main function that inserts a new key in this B-Tree 
	void insert(T key) 
	{ 
		// If tree is empty 
		if (root == NULL) 
		{ 
			// Allocate memory for root 
			root = new BTreeNode<T>(t, true); 
			root->keys[0] = key;  // Insert key 
			root->numKeys = 1;  // Update number of keys in root 
		} 
		else // If tree is not empty 
		{ 
			// If root is full, then tree grows in height 
			if (root->numKeys == 2*t-1) 
			{ 
				// Allocate memory for new root 
				BTreeNode<T> *s = new BTreeNode<T>(t, false); 
	  
				// Make old root as child of new root 
				s->childPtrs[0] = root; 
	  
				// Split the old root and move 1 key to the new root 
				s->splitChild(0, root); 
	  
				// New root has two children now.  Decide which of the 
				// two children is going to have new key 
				int i = 0; 
				if (s->keys[0] < key) 
					i++; 
				s->childPtrs[i]->insertNonFull(key); 
	  
				// Change root 
				root = s; 
			} 
			else  // If root is not full, call insertNonFull for root 
				root->insertNonFull(key); 
		} 
	} 
	
	/********************************************
	* Function Name  : remove
	* Pre-conditions : T key
	* Post-conditions: none
	* 
	* Find and remove the key from the B-tree 
	********************************************/
	void remove(T key) 
	{ 
		if (!root) 
		{ 
			std::cout << "The tree is empty\n"; 
			return; 
		} 
	  
		// Call the remove function for root 
		root->remove(key); 
	  
		// If the root node has 0 keys, make its first child as the new root 
		//  if it has a child, otherwise set root as NULL 
		if (root->numKeys==0) 
		{ 
			BTreeNode<T> *tmp = root; 
			if (root->leaf) 
				root = NULL; 
			else
				root = root->childPtrs[0]; 
	  
			// Free the old root 
			delete tmp; 
		} 
		return; 
	} 

}; 

#endif
